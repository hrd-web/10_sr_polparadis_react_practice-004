import React from "react"
import school from '../images/school.jpg'
import sport from '../images/sport.jpg'
import Instructor from '../images/Instructor.jpg'

function AboutUsComponent() {
  return (
    <div className="about">
      <div className="row">
        <div className='col'>
          <img src={Instructor} />
        </div>
        <div className='col'>
          <p>Phnom Penh: On Tuesday, April 12th, 2022, Korea Software HRD Center has held a Korean Cooking Competition and Sankran to celebrate the new year (the Year of the Tiger) with the participation of Deputy Director, Mr. Chen Phirum, the instructors and the 10th Generation students of Korea Software HRD Center.<br /><br />

            The Korean Cooking Competition took place in the morning, where the students were divided into teams and competed for the best dish. In the afternoon, everyone took part in many traditional Khmer games, which made the whole event more entertaining. <br /><br />

            Korea Software HRD Center wishes good blessings, happiness and prosperity to the management team, instructors and students, as well as all the partners of the center.</p>
        </div>
      </div>

      <div className="row">
        <div className='col'>
          <p>On March 19, 2022, Korea Software HRD Center held a sports event and monthly party for 10th Generation Basic Course Students for March 2022.<br /><br />

            The Sports Event was organized on purpose of building solidarity, friendship, and staying healthy with the participants of Director, Deputy Director, staff and students from 10th Generation Basic Course. We also had a photo session together at the end of the Sport Event.<br /><br />

            In the evening, everyone also had BBQ dinner together. The event was going smoothly and was full of entertainment.</p>
        </div>
        <div className='col'>
          <img src={sport} alt={sport} />
        </div>
      </div>

      <div className="row">
        <div className='col'>
          <img src={school} />
        </div>
        <div className='col'>
          <p>On February 28, 2022,  Korea Software HRD Center hosted an Opening and Orientation Event and AEU golden building and by online to welcome ITE 10th Generation Basic Course, chairmanship by Director Mr. KIM HAN-SOO, Deputy Director Mr. CHEN PHIRUM, all staff and ITE 10th Generation Basic Course students. <br /><br />

            The Event included welcome speech and good advice to all students in 10th Generation Advanced Course from the Director and Deputy Director. There was also a presentation about the rule and regulation, the assessment of each subject of the 10th Generation Basic Course including Korean Language. <br /><br />

            At the end of the event, there were also questions and answers and a photo session together.</p>
        </div>
      </div>
    </div>
  )
}

export default AboutUsComponent;

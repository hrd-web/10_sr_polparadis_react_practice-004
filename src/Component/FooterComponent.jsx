import React from 'react'
import logo from '../images/logo.png'

function FooterComponent() {
  return (
    <div className='footer'>
      <div className="row text-center">
        <div className='col'>
          <img src={logo} />
          <p>រក្សាសិទ្ធិគ្រប់ដោយ KSHRD Center ឆ្នាំ ២០២២</p>
        </div>

        <div className='col'>
          <h5>Address</h5>
          <p><b>Address:</b> #12, St 323, Sangkat Boeung Kak II, Khan Toul Kork, Phnom Penh, Cambodia.</p>
        </div>

        <div className='col'>
          <h5>Contact</h5>
          <p>
            <b>Tel:</b> 012 998 919(Khmer)<br />
            <b>Tel:</b> 085 402 605(Korean)<br />
            <b>Email:</b> info.kshrd@gmail.com phirum.gm@gmail.com
          </p>
        </div>

      </div>
    </div>
  )
}

export default FooterComponent;

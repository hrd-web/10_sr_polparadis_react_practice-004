import React from 'react'
import { Container, Button, Row, Col, Card } from 'react-bootstrap';
function VisionComponent() {
  return (
    <div className="vision">
      <Container>
        <Row>
          <Col xs={12} md={6} lg={6} sm={12} className='mt-5'>
            <Card style={{ height: '300px' }} className='bg-light'>
              <Card.Body>
                <Card.Title >Vision</Card.Title>
                <Card.Text as="li" className="text-start">
                  To be the best SW Professional Training Center in Cambodia.
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col xs={12} md={6} lg={6} sm={12} className='mt-5'>
            <Card style={{ height: '300px' }} className='bg-light' >
              <Card.Body>
                <Card.Title>Mission</Card.Title>
                <Card.Text as="li" className="text-start"> Heigh quality traning and research
                </Card.Text>
                <Card.Text as="li" className="text-start">
                  Developing capacity of SW Expert to be Leader in IT Field
                </Card.Text>
                <Card.Text as="li" className="text-start">
                  Developing sustainable ICT Program
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col xs={12} md={6} lg={6} sm={12} className='mt-4' >
            <Card style={{ height: '300px' }} className='bg-light'>
              <Card.Body>
                <Card.Title>Strategy</Card.Title>
                <Card.Text as="li" className="text-start"> Best traning method with up to date curriculum and enviroment
                </Card.Text>
                <Card.Text as="li" className="text-start">
                  Cooperation with the best IT industry to guraantee student's career and benefits.
                </Card.Text>
                <Card.Text as="li" className="text-start">
                  Additional Soft Skill, Management, Leadership traning
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col xs={12} md={6} lg={6} sm={12} className='mt-4 '>
            <Card style={{ height: '300px' }} className='bg-light'>
              <Card.Body>
                <Card.Title>Slogan</Card.Title>
                <Card.Text as="li" className="text-start">
                  "KSHRD, connect you to various opportunities in IT Field".

                </Card.Text>
                <Card.Text as="li" className="text-start">
                  Raising brand awareness with continous advertisment of SNS and of ather media
                </Card.Text>
                <Card.Text as="li" className="text-start">
                  Developing sustainable ICT Program
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default VisionComponent;
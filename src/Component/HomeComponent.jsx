import React from 'react'
import { Button, Card, Col, Container, Row } from 'react-bootstrap';

export default function HomeComponent({ items }) {
  return (
    <div>
      <Container>
        <h2>Trending Courses</h2>
        <Row>
          {items.map((item, index) => (
            <Col xs={12} md={6} lg={3} sm={12} key={index} className='mt-2'>
              <Card>
                <Card.Img variant="top" src={item.thumbnail} />
                <Card.Body className='text-center'>
                  <Card.Title>{item.title}</Card.Title>
                  <Card.Text>{item.description} </Card.Text>
                  <Card.Text>
                    <h4 className='text-danger'>{item.price}</h4>
                  </Card.Text>
                  <Button variant="success">Buy the course</Button>
                </Card.Body>
              </Card>
            </Col>
          )
          )}
        </Row>
      </Container>
    </div>
  )
}
import React from 'react'
import { Link, NavLink } from 'react-router-dom';
import { Navbar, Nav, Container, Button } from 'react-bootstrap';
function NavbarComponent() {
    return (
        <div className="Container-fluid">
            <Navbar bg="light" expand="lg" fixed="top" >
                <Container>
                    <Navbar.Brand as={NavLink} to="/">PT-004</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav ">
                        <Nav className="me-auto w-100 d-flex justify-content-end">
                            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                            <Nav.Link as={NavLink} to="/About">About Us</Nav.Link>
                            <Nav.Link as={NavLink} to="/Vision">Our vision</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}
export default NavbarComponent;
